import java.lang.Math;

class Solution {
    public int maxArea(int[] height) {
        int maxArea = 0;
        int left = 0;
        int right = height.length - 1;
        
        while (left < right) {
            int minHeight = Math.min(height[left], height[right]);
            int currentArea = (right - left) * minHeight;
            maxArea = Math.max(maxArea, currentArea);
            
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        
        return maxArea;
    }
}

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        // Example 1
        int[] height1 = {1, 8, 6, 2, 5, 4, 8, 3, 7};
        System.out.println("Example 1: " + solution.maxArea(height1)); // Output: 49
        
        // Example 2
        int[] height2 = {1, 1};
        System.out.println("Example 2: " + solution.maxArea(height2)); // Output: 1
    }
}
